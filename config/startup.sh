#!/bin/bash
CONFDIR='/app/config'
MSGCONVDIR='/app/msgconvert'

apt-get update
apt-get install -y git

if [ ! -d /app/msgconvert/ ]
then
	/usr/bin/git clone https://github.com/mvz/email-outlook-message-perl.git $MSGCONVDIR
fi
cd $MSGCONVDIR
perl ./Build.PL

#Config CPAN
(echo y;echo o conf prerequisites_policy follow;echo o conf commit)|cpan
./Build installdeps
./Build install

echo -e "To use: vagrant ssh \nAnd run msgconvert"
