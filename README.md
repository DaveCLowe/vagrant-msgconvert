# Vagrant file for MsgConvert
A simple Vagrant setup to download and install msgconvert.

The purpose of this tool is to easily convert Outlook email messages (msg) to mbox text format.

Shares pwd as /app within the virtual machine. 

## Installation
* Clone this project and change into vagrant-msgconvert
* Copy your Outlook msg to pwd (Shared as /app in VM)
* vagrant up :)

## Usage
* vagrant ssh
* msgconvert --help
* msgconvert --outfile /app/outfile.txt /app/email.msg

### Thanks
Author of the perl fu that handles the msg conversion: Matijs van Zuijlen
https://github.com/mvz/email-outlook-message-perl
